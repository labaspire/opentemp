OpenTEMP v1.1.3
12 Mart, 2018

Bu program bilgisayarlar ve çeşitli onewire sıcaklık sensörlenden gelen bilgilerin otomatik olarak kaydedilmesi 
ve bir web arayüzü üzerinden kullanıcıları sunulması için hazırlanmıştır. Kullanıcının tek yapması gereken config 
dosyasının isteğe göre düzenlemektir. 

Config dosyasının içeriğini değiştirerek yeni cihaz ve sensör tanımlaması yapılabilir.
Örnek bir config dosyası içeriği aşağıda verilmiştir

Bu düzende:

[user] kısmına config dosyasını oluşturan kişinin adı ve firma ismi bilgilendirme amaçlı olarak yazılmalıdır. 
[hardware] kısmına sıcaklık takibi yapılacak bilgisayar donanımları için bilgiler girilmelidir:
        hostnameList - bilgisayar adı
        usernameList - bilgisayar kullanıcı adı
        passwordList - bilgisayar kullanıcı adı şifresi
        coreList - bilgisayarın kaç çekirdekli olduğu bilgisi
        NOT: bu kısımda girilen kullanıcı özel olarak bu sensör için yaratılmalı ve sadece bu amaç için                 
        kullanılmalıdır. AKsi taktirde güvenlik problemleri oluşabilir. 

[sensors] kısmına kullanılan onewire sıcaklık sensörlerinin bilgileri eklenmelidir:
        sensorAreaList - sensörün bulunduğu konum bilgisi
        sensorIdList - onewire sensörlerin adres bilgisi 
        lineColorList - sensörlerin grafiğe yansıtılmasında kullanıalcak renk kodu

[connection] kısmına onewire bağlantısının hangi port üzerinden yapılacağı bilgisi

[directories] kısmına programın ve program çalışırken otomatikoluşturacağı klasörlerin bilgisi (sadece gerektiğinde değiştiriniz)

[fnames] onewire sensörlerin veritabanı ismi

[times] rrd dosyasının çözünürlük bilgisi, saniye cinsinden (300 için 5 dakikadır)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Config Başla
[user]
name =user
company =company


[hardwares]
hostnameList =dagobah,mustafar,endor,naboo,tatooine
usernameList =radar,radar,radar,radar,radar
passwordList =aspire,aspire,aspire,aspire,aspire
coreList 	 = 4,4,12,12,12


[sensors]
sensorAreaList =Outside Temperature,Room Temperature 1,Room Temperature 2
sensorIdList   =/28.B8A8D0050000,/28.7EB5D0050000,/28.C7483C040000
lineColorList =#FF0000,#00FF00,#0000FF


[connection]
usbPort =usb2


[directories]
libraries =lib
images =img
figures =fig
websit =web
database=db


[fnames]
fname =tempsensor.rrd


[times]
getData_res=300

% Config Bitiş
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
