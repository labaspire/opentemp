#/usr/bin/env/python
# -*- coding: utf-8 -*-

"""
 ██████╗ ██████╗ ███████╗███╗   ██╗████████╗███████╗███╗   ███╗██████╗ 
██╔═══██╗██╔══██╗██╔════╝████╗  ██║╚══██╔══╝██╔════╝████╗ ████║██╔══██╗
██║   ██║██████╔╝█████╗  ██╔██╗ ██║   ██║   █████╗  ██╔████╔██║██████╔╝
██║   ██║██╔═══╝ ██╔══╝  ██║╚██╗██║   ██║   ██╔══╝  ██║╚██╔╝██║██╔═══╝ 
╚██████╔╝██║     ███████╗██║ ╚████║   ██║   ███████╗██║ ╚═╝ ██║██║     
 ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝     
~   v-1.1.3
~   Applied Signal Processing and Insturmentation Lab.
~   labaspire.com
"""
                                                                       
import numpy as np
import rrdtool
import ow
import sys,os,time,datetime
from rrdtool import update as rrd_update
import subprocess
from multiprocessing import Process
sys.path.append('lib')
from functions import *
from configparser import SafeConfigParser
sys.path.append('web')
from serve import server_net

def main():

    # Server baslat 
    sp=Process(target=server_net)
    sp.start()

    # Yapilandirma dosyasini oku
    config = SafeConfigParser()
    config.read("configuration/opentemp.cfg")
    config.sections()

    # Kayitli adresleri al
    hostnameList = config.get("hardwares","hostnameList")
    hostnames = hostnameList.split(",")

    # Kayitli kullanici adlarini al
    usernameList = config.get("hardwares","usernameList")
    usernames = usernameList.split(",")

    # Kayitli cekirdek sayilarini al
    coreList = config.get("hardwares","coreList")
    cores = coreList.split(",")

    # Kayitli sifreleri al
    passwdList = config.get("hardwares","passwordList")
    passwords = passwdList.split(",")

    # Kayitli sensor konumlarini al
    sensorAreaList = config.get("sensors","sensorAreaList")
    areas = sensorAreaList.split(",")

    # Kayitli sensor adlarini al
    sensorIdList = config.get("sensors","sensorIdList")
    ids = sensorIdList.split(",")
    
    # Kayitli renkleri al
    lineColorList = config.get("sensors","lineColorList")
    colors = lineColorList.split(",")

    # Kayitli usb portunu al
    portUSB = config.get("connection","usbPort")
    usbPort = str(portUSB)

    # Kayitli lokasyonlari al
    libdir = str(config.get("directories","libraries"))

    imgdir = str(config.get("directories","images"))

    figdir = str(config.get("directories","figures"))

    dbdir = str(config.get("directories","database"))

    webdir = str(config.get("directories","websit"))

    fname = str(config.get("fnames","fname"))

    getData_res = int(config.get("times","getData_res"))



    # Alttaki kisim yapilandirma dosyasini test eder
    again_hard = False
    again_ow = False
    if len(hostnames) == len(cores) == len(usernames) == len(passwords):
        again_hard = False
    else:
        again_hard = True

    if len(areas) == len(ids) == len(colors):
        again_ow = False
    else:
        again_ow = True    

    if again_hard or again_ow == True:
        print(">>> Config file error !")
        sys.exit()
    else:
        pass

        

    # Adresler ve cekirdek sayilarini iliskilendir
    hostname_coreDict = {}
    for HwareNo in np.arange(0,len(hostnames)):
	    hostname_coreDict[str(hostnames[HwareNo])]=int(cores[HwareNo])

    # Adresler ve kullanici adlarini iliskilendir
    hostname_usernameDict = {}
    for HwareNo in np.arange(0,len(hostnames)):
	    hostname_usernameDict[str(hostnames[HwareNo])]=str(usernames[HwareNo])
	    
    # Adresler ve sifreleri sayilarini iliskilendir
    hostname_passDict = {}
    for HwareNo in np.arange(0,len(hostnames)):
	    hostname_passDict[str(hostnames[HwareNo])]=str(passwords[HwareNo])

    # Sensorler ve sensor konumlarini iliskilendir
    sensor_areaDict = {}
    for sensorNo in np.arange(0,len(areas)):
	    sensor_areaDict[str(ids[sensorNo])]=str(areas[sensorNo])



    # Sensor veritabani dosyalarini kontrol et
    if os.path.isfile(dbdir+"/"+fname):
        print("File " +fname + " already exists")
        
        os.chdir(dbdir)
        db_info = rrdtool.fetch(fname, "AVERAGE")
        db_sensors = db_info[1]
        conf_sensors = areas
        count = 0
        
        if len(db_sensors) == len(conf_sensors):
            for dbs in db_sensors:
                for confs in conf_sensors:
                    if str(dbs) == (str(confs)+"temp") :
                        count = count+1
            if count == len(db_sensors):
                pass
                
            elif count > len(db_sensors):
                print(">>> Could not give same name ow sensors")
                time.sleep(2)
                sys.exit()
            else:
                print(">>> User changed configuration file")
                print(">>> Updates applying")
                print(">>> Old .rrd file backup")
                an = datetime.datetime.now()
                date = datetime.datetime.strftime(an,'%X%a%b%y')
                # Eski veritabanini yedekle
                os.system("mv "+fname+" "+os.getcwd()+"/../backups/"+date+fname)
                print(">>> Creating new .rrd file")
                # Yeni veritabani olustur 
                rrdFile=createRrdFileForSensors(fname)           
                print(">>> Done !")
        os.chdir("../")
    else:
        os.chdir(dbdir)
        # Sensorler icin veritabani dosyasi olustur
        rrdFile=createRrdFileForSensors(fname)
        os.chdir("../")
	    
	# Donanimlarin veritabani ve web arayuzu dosyalarini duzenle    
    for hostname, core  in sorted(hostname_coreDict.iteritems()):
        fnamehost=hostname+".rrd"

        if os.path.isfile(dbdir+"/"+fnamehost):
            print("File " +fnamehost + " already exists")
            if os.path.isfile(webdir+"/"+hostname+".html"):
                print("File "+ hostname + ".html already exists")
            else:
                #Create hostname.html file for new hardware
                os.chdir(webdir)
                # Yeni eklenen sensorler icin oto web arayzuzu olustur
                os.system("cp template.html "+hostname+".html")
                # Yeni olusturulan web arayuzunu duzenle
                edit_html(hostname,len(hostnames))
                os.chdir("../")
        else:
            os.chdir(dbdir)
            # Islemciler icin veritabani dosyalarini olustur
            createRrdFileCPU(fnamehost,int(core))
            os.chdir("../")
        os.chdir(webdir)
        # Index.html dosyasini guncelle
        update_index(hostnames)
        os.chdir("../")


            
    
    ow.init(usbPort)
    
    # Ana donguyu baslat
    while True:
        # Sensor verilerini al
	    TemperatureList={}
	    for i in ow.Sensor("/").sensorList():
	        sensorName = str(i).split(" - ")[0]
	        if sensorName in sensor_areaDict:
	            TemperatureList[sensor_areaDict.get(sensorName, 0)] = ow.Sensor(sensorName).temperature
	    os.chdir(dbdir)
	    n_of_sensor = len(ids)
	    s = ':%s'
	    # Veritabani dosyalarini guncelle
	    rrd_update(fname,('N'+(n_of_sensor*s))%(tuple(TemperatureList.values())));
	    # Veri grafiklerini olustur
	    drawSensorGraph(fname)
	    
	    
	    for hostname, core  in sorted(hostname_coreDict.iteritems()):
		    core=int(core)
		    fnamehost=hostname+".rrd"
		    username = hostname_usernameDict[hostname]
	        # Islemci verilerini al
		    temps = getRemoteCPUTemp(hostname, username, hostname_passDict)
		    # Islemci veritabanlarini guncelle
		    rrdUpdateCPU(fnamehost,core,temps)
		    # Islemci veri grafiklerini olustur
		    if core==2:
			    drawRemoteCPU2Graph(hostname)
		    elif core==4:
			    drawRemoteCPU4Graph(hostname)
		    elif core==8:
			    drawRemoteCPU8Graph(hostname)
		    elif core==12:
			    drawRemoteCPU12Graph(hostname)
		    elif core==16:
			    drawRemoteCPU16Graph(hostname)
		    else:
			    print("Undefined core number.. Please update software.")


	    os.chdir("../")
        # Guncelleme cozunurlugu
	    time.sleep(getData_res)

if __name__ == "__main__":
     main()


