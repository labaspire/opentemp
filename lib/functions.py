#/usr/bin/env/python
# -*- coding: utf-8 -*-
"""

███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
█████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                                                          

"""
import numpy as np
import ow
import sys,os
import rrdtool
from rrdtool import update as rrd_update
import subprocess
import paramiko
import getpass
import time, datetime
from configparser import SafeConfigParser

# Sensorler icin veritabani dosyalarini olustur 
def createRrdFileForSensors(fname):
    
    # Yapilandirma dosyasini oku
    config = SafeConfigParser()
    config.read("../configuration/opentemp.cfg")
    config.sections()

    # Kayitli sensor konumlarini al
    sensorAreaList = config.get("sensors","sensorAreaList")
    areas = sensorAreaList.split(",")
    
    
    sensors = []
    for i in np.arange(len(areas)):
        sensor = "DS:"+str(areas[i])+"temp:GAUGE:600:-40:60"
        sensors.append(sensor)   
         
    # Gunluk 5 dk cozunurluk
    # Haftalik 15 dk cozunurluk
    # Aylik 1 sa cozunurluk
    # Yillik 6 sa cozunurluk
    
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    sensors,
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created rrd file for temperature sensors"
    return rrdFile

# Donanimlar icin veritabani dosyalarini olustur
def createRrdFileCPU(fname,core):
    if core == 2:
        create_2_coreRrd(fname)
    elif core == 4:
        create_4_coreRrd(fname)
    elif core == 8:
        create_8_coreRrd(fname)
    elif core == 12:
        create_12_coreRrd(fname)
    elif core == 16:
        create_16_coreRrd(fname)
    else:
        print("Undefined hardware")
        

# CPU veritabani olusturan fonksiyon
def create_2_coreRrd(fname):
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    "DS:Core1temp:GAUGE:600:0:120",
    "DS:Core2temp:GAUGE:600:0:120",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created",fname," rrd file."

# CPU veritabani olusturan fonksiyon
def create_4_coreRrd(fname):
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    "DS:Core1temp:GAUGE:600:0:120",
    "DS:Core2temp:GAUGE:600:0:120",
    "DS:Core3temp:GAUGE:600:0:120",
    "DS:Core4temp:GAUGE:600:0:120",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created",fname," rrd file."

# CPU veritabani olusturan fonksiyon
def create_8_coreRrd(fname):
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    "DS:Core1temp:GAUGE:600:0:120",
    "DS:Core2temp:GAUGE:600:0:120",
    "DS:Core3temp:GAUGE:600:0:120",
    "DS:Core4temp:GAUGE:600:0:120",
    "DS:Core5temp:GAUGE:600:0:120",
    "DS:Core6temp:GAUGE:600:0:120",
    "DS:Core7temp:GAUGE:600:0:120",
    "DS:Core8temp:GAUGE:600:0:120",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created",fname,"rrd file."

# CPU veritabani olusturan fonksiyon
def create_12_coreRrd(fname):
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    "DS:Core1temp:GAUGE:600:0:120",
    "DS:Core2temp:GAUGE:600:0:120",
    "DS:Core3temp:GAUGE:600:0:120",
    "DS:Core4temp:GAUGE:600:0:120",
    "DS:Core5temp:GAUGE:600:0:120",
    "DS:Core6temp:GAUGE:600:0:120",
    "DS:Core7temp:GAUGE:600:0:120",
    "DS:Core8temp:GAUGE:600:0:120",
    "DS:Core9temp:GAUGE:600:0:120",
    "DS:Core10temp:GAUGE:600:0:120",
    "DS:Core11temp:GAUGE:600:0:120",
    "DS:Core12temp:GAUGE:600:0:120",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created",fname,"rrd file."


# CPU veritabani olusturan fonksiyon
def create_16_coreRrd(fname):
    rrdFile=rrdtool.create(fname,
    "--step","300","--start",'0',
    "DS:Core1temp:GAUGE:600:0:120",
    "DS:Core2temp:GAUGE:600:0:120",
    "DS:Core3temp:GAUGE:600:0:120",
    "DS:Core4temp:GAUGE:600:0:120",
    "DS:Core5temp:GAUGE:600:0:120",
    "DS:Core6temp:GAUGE:600:0:120",
    "DS:Core7temp:GAUGE:600:0:120",
    "DS:Core8temp:GAUGE:600:0:120",
    "DS:Core9temp:GAUGE:600:0:120",
    "DS:Core10temp:GAUGE:600:0:120",
    "DS:Core11temp:GAUGE:600:0:120",
    "DS:Core12temp:GAUGE:600:0:120",
    "DS:Core13temp:GAUGE:600:0:120",
    "DS:Core14temp:GAUGE:600:0:120",
    "DS:Core15temp:GAUGE:600:0:120",
    "DS:Core16temp:GAUGE:600:0:120",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    print "Created",fname,"rrd file."

# Index.html dosyasini guncelle
def update_index(hostnames):

    # index.html dosyasini oku
    with open('index.html', 'r') as file:
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    links = []    
    for x in data:
        link = find_between(x,'href="','.html')
        if link != "":
            links.append(link)
        else:
            pass
            
            
    # index.html icindeki eski linkleri sil        
    for link in links:
        try:
            data.remove('		<a href="'+link+'.html" title="'+link+'" ><div class="nav">'+link+'</div></a>\n')
            
        except:
            pass        
            
            
    # index.html icine yeni linkleri ekle
    for hostname in hostnames:
        newHardware = '		<a href="'+hostname+'.html" title="'+hostname+'" ><div class="nav">'+hostname+'</div></a>\n'
        data.insert(20,newHardware)        
            


    # Yeni index.html dosyasini kaydet
    with open('index.html', 'w') as file:
        file.writelines( data )
        
        
        
        

# Donanimlar icin .html dosyalarini guncelle
def edit_html(hostname,number):

    # Donanimin .html dosyasini oku 
    with open(hostname+'.html', 'r') as file:
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    # Dosya icindeki duzenlemeler
    
    data[30] ='         <li><a href="#daily">Daily</a><img id="daily" src="../fig/'+hostname+'dailygraph.png"/></li>\n'
    data[31] ='         <li><a href="#weekly">Weekly</a><img id="weekly" src="../fig/'+hostname+'weeklygraph.png"/></li>\n'
    data[32] ='         <li><a href="#monthly">Monthly</a><img id="monthly" src="../fig/'+hostname+'monthlygraph.png"/></li>\n'
    data[33] ='         <li><a href="#yearly">Yearly</a><img id="yearly" src="../fig/'+hostname+'yearlygraph.png"/></li>\n'
    
    
    
    # Yeni .html dosyasini kaydet
    with open(hostname+'.html', 'w') as file:
        file.writelines( data )
    







# Veritabanindaki cpu verilerini guncelle
def rrdUpdateCPU(fnamehost,core,temps):

    if core==2:
        rrd_update(fnamehost,'N:%s:%s'%(tuple(temps)));
    elif core==4:
        rrd_update(fnamehost,'N:%s:%s:%s:%s'%(tuple(temps)));
    elif core==8:
        rrd_update(fnamehost,'N:%s:%s:%s:%s:%s:%s:%s:%s'%(tuple(temps)));
    elif core==12:
        rrd_update(fnamehost,'N:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s'%(tuple(temps)));
    elif core==16:
        rrd_update(fnamehost,'N:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s'%(tuple(temps)));








# Ilgili sensorden veri al
def getTemp(sensorid):

    temp = ow.Sensor(sensorid)
    return temp.temperature








# Bir str dizide belirli indexleri bulan fonksiyon
def find_between( s, first, last ):

    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""
    





# Donanimlara baglanarak veri alan fonksiyon
def getRemoteCPUTemp(ip, uname, hostname_passDict):



        ssh = paramiko.SSHClient()

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        p = hostname_passDict[ip]
        
        ssh.connect(ip, username=uname, password=p)

        stdin, stdout, stderr = ssh.exec_command('sensors | grep "Core"')
        
        data = stdout.read()
        
        data = data.split('\n')         
        
        cpuTemps =[]
        
        for line in data[0:-1]:
        
            cpuTemp = float(find_between(line,"+",u" C"))
            cpuTemps.append(cpuTemp)

        return cpuTemps
        


# Sensor grafiklerini cizdiren fonksiyon
def drawSensorGraph(fname):

        lastupdated = rrdtool.last(fname)
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        
        
        config = SafeConfigParser()
        config.read("../configuration/opentemp.cfg")
        config.sections()
        
        
        
        ColorList = config.get("sensors","lineColorList")
        lineColors = ColorList.split(",")



        sensorAreaList = config.get("sensors","sensorAreaList")
        areas = sensorAreaList.split(",")
        
        area_colorDict = {}
        for sensorNo in np.arange(0,len(areas)):
            area_colorDict[str(areas[sensorNo])]=str(lineColors[sensorNo])
        
        sensors = []
        i = 0
        for i in np.arange(len(areas)):
            deff = "DEF:"+str(areas[i])+"temps="+fname+":"+str(areas[i])+"temp:AVERAGE"
            line = "LINE1:"+str(areas[i])+"temps"+area_colorDict[areas[i]]+":"+str(areas[i])
            gprint_min = "GPRINT:"+str(areas[i])+"temps:MIN:Min "+str(areas[i])+" Temperature\: %6.2lfC"
            comment_empty = "COMMENT:  "
            gprint_max = "GPRINT:"+str(areas[i])+"temps:MAX:Max "+str(areas[i])+" Temperature\: %6.2lfC\\r"
            comment_bottom = "COMMENT:\\n"
            sensors.append(deff)
            sensors.append(line)
            sensors.append(gprint_min)
            sensors.append(comment_empty)
            sensors.append(gprint_max)
            sensors.append(comment_bottom)
                
        for z in ["daily","weekly","monthly","yearly"]:

			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/sensordailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title=Sensor Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                sensors,
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/sensorweeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title=Sensor Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                sensors,                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/sensormonthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title=Sensor Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                sensors,                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/sensoryearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title=Sensor Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                sensors,                
                "COMMENT: Last Updated\: %s" %lastupdatedf)





# CPU grafiklerini cizdiren fonksiyon
def drawRemoteCPU2Graph(hostname):

        lastupdated = rrdtool.last(hostname+".rrd")
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        for z in ["daily","weekly","monthly","yearly"]:
			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/"+hostname+"dailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/"+hostname+"weeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)				
			if z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/"+hostname+"monthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)	
			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/"+hostname+"yearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)





# CPU grafiklerini cizdiren fonksiyon
def drawRemoteCPU4Graph(hostname):
        lastupdated = rrdtool.last(hostname+".rrd")
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        for z in ["daily","weekly","monthly","yearly"]:
			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/"+hostname+"dailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/"+hostname+"weeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)				
			if z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/"+hostname+"monthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)	
			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/"+hostname+"yearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "LINE1:Core1temp#00FF00:Core Temperature 1",
                "LINE1:Core2temp#FF0000:Core Temperature 2",
                "LINE1:Core3temp#0000FF:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)





# CPU grafiklerini cizdiren fonksiyon
def drawRemoteCPU8Graph(hostname):
        lastupdated = rrdtool.last(hostname+".rrd")
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        for z in ["daily","weekly","monthly","yearly"]:
			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/"+hostname+"dailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/"+hostname+"weeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label="+hostname+" Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/"+hostname+"monthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/"+hostname+"yearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)



# CPU grafiklerini cizdiren fonksiyon
def drawRemoteCPU12Graph(hostname):
        lastupdated = rrdtool.last(hostname+".rrd")
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        for z in ["daily","weekly","monthly","yearly"]:
			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/"+hostname+"dailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/"+hostname+"weeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label="+hostname+" Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/"+hostname+"monthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/"+hostname+"yearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)





# CPU grafiklerini cizdiren fonksiyon
def drawRemoteCPU16Graph(hostname):
        lastupdated = rrdtool.last(hostname+".rrd")
        lastupdatedf = datetime.datetime.fromtimestamp(int(lastupdated)).strftime('%Y-%m-%d %H\:%M\:%S')
        for z in ["daily","weekly","monthly","yearly"]:
			if z=="daily":
				period = "d"
				draw=rrdtool.graph("../fig/"+hostname+"dailygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="weekly":
				period = "w"
				draw=rrdtool.graph("../fig/"+hostname+"weeklygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label="+hostname+" Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

			elif z=="monthly":
				period = "m"
				draw=rrdtool.graph("../fig/"+hostname+"monthlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)
			elif z=="yearly":
				period = "y"
				draw=rrdtool.graph("../fig/"+hostname+"yearlygraph.png","--start","-1%s"%(period),"--width=800", 
                "--title="+hostname+" Temperature Monitor","--watermark=ASPIRE LAB", "--vertical-label=Temperature (C)", "--height=300",
                "DEF:Core1temp="+hostname+".rrd"":Core1temp:AVERAGE",
                "DEF:Core2temp="+hostname+".rrd"":Core2temp:AVERAGE",
                "DEF:Core3temp="+hostname+".rrd"":Core3temp:AVERAGE",
                "DEF:Core4temp="+hostname+".rrd"":Core4temp:AVERAGE",
                "DEF:Core5temp="+hostname+".rrd"":Core5temp:AVERAGE",
                "DEF:Core6temp="+hostname+".rrd"":Core6temp:AVERAGE",
                "DEF:Core7temp="+hostname+".rrd"":Core7temp:AVERAGE",
                "DEF:Core8temp="+hostname+".rrd"":Core8temp:AVERAGE",
                "DEF:Core9temp="+hostname+".rrd"":Core9temp:AVERAGE",
                "DEF:Core10temp="+hostname+".rrd"":Core10temp:AVERAGE",
                "DEF:Core11temp="+hostname+".rrd"":Core11temp:AVERAGE",
                "DEF:Core12temp="+hostname+".rrd"":Core12temp:AVERAGE",
                "LINE1:Core1temp#00FFFF:Core Temperature 1",
                "LINE1:Core2temp#FF00FF:Core Temperature 2",
                "LINE1:Core3temp#CD5C5C:Core Temperature 3",
                "LINE1:Core4temp#000000:Core Temperature 4",
                "LINE1:Core5temp#00FF00:Core Temperature 5",
                "LINE1:Core6temp#FF0000:Core Temperature 6",
                "LINE1:Core7temp#0000FF:Core Temperature 7",
                "LINE1:Core8temp#800000:Core Temperature 8",
                "LINE1:Core9temp#808000:Core Temperature 9",
                "LINE1:Core10temp#8B4513:Core Temperature 10",
                "LINE1:Core11temp#66CDAA:Core Temperature 11",
                "LINE1:Core12temp#CD853F:Core Temperature 12\\r",
                "COMMENT:\\n",
                "GPRINT:Core1temp:MIN:Min Core 1 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core1temp:MAX:Max Core 1 Temperature\: %6.2lfC\\r",                
                "COMMENT:\\n",
                "GPRINT:Core2temp:MIN:Min Core 2 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core2temp:MAX:Max Core 2 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core3temp:MIN:Min Core 3 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core3temp:MAX:Max Core 3 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core4temp:MIN:Min Core 4 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core4temp:MAX:Max Core 4 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core5temp:MIN:Min Core 5 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core5temp:MAX:Max Core 5 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core6temp:MIN:Min Core 6 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core6temp:MAX:Max Core 6 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core7temp:MIN:Min Core 7 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core7temp:MAX:Max Core 7 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core8temp:MIN:Min Core 8 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core8temp:MAX:Max Core 8 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core9temp:MIN:Min Core 9 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core9temp:MAX:Max Core 9 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core10temp:MIN:Min Core 10 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core10temp:MAX:Max Core 10 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core11temp:MIN:Min Core 11 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core11temp:MAX:Max Core 11 Temperature\: %6.2lfC\\r",
                "COMMENT:\\n",
                "GPRINT:Core12temp:MIN:Min Core 12 Temperature\: %6.2lfC",
                "COMMENT:  ",
                "GPRINT:Core12temp:MAX:Max Core 12 Temperature\: %6.2lfC\\r",                
                "COMMENT: Last Updated\: %s" %lastupdatedf)

    


